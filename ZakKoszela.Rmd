---
title: "<h1><b>Jakie czynniki mają wpływ na cenę samochodów segmentu C? Wielokryterialna analiza wpływu pochodzenia (polskie/zagraniczne), modelu, typu, roku produkcji, stanu oraz bogactwa wyposażenia na cenę"

author: "Wykonali : Katarzyna Żak 168105, Wojciech Koszela 210219<b></h1>"
date: "Data : 31.05.2020"
output:
    html_document: 
      number_sections: yes
      theme: united
      toc: yes
      toc_float: yes
      dev: svg
      highlight: haddock
---

```{r setup, include=FALSE}
library(knitr)
opts_chunk$set(echo = TRUE, 
               cache = FALSE,
               prompt = FALSE,
               tidy = TRUE,
               comment = NA,
               message = FALSE,
               warning = FALSE,
               fig.width = 7.4,
               fig.height = 4.7,
               fig.align = "center")
opts_knit$set(width = 75)
```

<style> 

tr {font-family: 'Fira Mono'; font-size:90%}
code, kbd,pre,samp {font-family: 'Fira Mono';
                    background-color: rgba(237, 125, 49, 0.04)}
body, h1, h2, h3, h4, h5 {font-family: 'Fira Sans'}

</style>

<p>
<figure>
    <img src='image/background.png' alt='Background' width='110%' />
</figure>
</p>

Projekt przygotowano w ramach wykładu "Statystyka matematyczna i ekon." prowadzonego przez dr. Roberta Kapłona. Projekt dotyczy zbioru danych z serwisu OTOMOTO.pl. Dane zostały pobrane w dniu 05.05.2020 rok. Analiza statystyczna została wykonana za pomocą języka programowania R wraz z potrzebnymi dodatkowymi bibliotekami opisanymi w punkcie 1.1.

Celem projektu jest próba odpowiedzi na pytanie, jak kształtuje się cena samochodów segmentu C oferowanych w serwisie internetowym OTOMOTO.pl na podstawie analizy wielokryterialnej czynników takich jak:
* wielokr wpływu pochodzenia (polskie/zagraniczne),
* model,
* typ,
* roku produkcji,
* przebieg pojazdu,
* stan pojazdu,
* wyposażenie pojazdu,

# Rozdział 1 - Wczytanie danych i odpowiednie ich przygotowanie.

## Wczytanie pakietów

Przed wczytaniem pakietów należy zainstalować i odpowiednio za pomocą poniższej komendy załadować.

Wczytanie potrzebnych pakietów.

```{r}
# Wczytanie grupy pakietów / bibliotek
library(tidyverse)
library(stringi)
library(ggplot2)
library(formatR)
library(ggthemes)
library(e1071)
library(knitr)
library(tidyr)
library(ggpubr)
```
## Wczytywanie danych

Mając dostępne zainstalowane oraz załadowane biblioteki, do dalszej kompilacji programu należy załadować wejściowy plik z bazą danych samochodów z serwisu OtoMoto.pl z dnia 5 maj 2020 r. Plik należy umieścić w roboczym miejscu danego projektu (otomoto0505_2020.csv). Plik ten różni się od oryginalnego brakiem polskich znaków. Baza posiada 320606 rekordów.

```{r}
loadedCarFromData <- read.csv("otomoto0505_2020.csv", na = "NA")
```

Przykładowa reprezentacja tabeli z samochodami została zaprezentowana poniżej (UWAGA w tabeli nie uwzględniono wszystkich atrybutów).

```{r, layout="l-body-outset"}
library(knitr)
kable(head(loadedCarFromData[1:5,c(1,2,3,4,13,14,15,16)]))
```

## Przygotowanie danych
W dostępnych zbiorze danych, wyposażenie pojazdów nie zostało zapisane w oddzielonych kolumnach. Należy zatem 70 elementów wyposażenia zapisać w osobnych kolumnach (zakładając, że dany pojazd posiada element wyposażenia)


### Przypisanie każdemu pojazdowi elementów wyposażenia oraz zapisanie w końcowym obiekcie reperezentującym wszystkie pojazdu wraz z rozdzielonym wyposażeniem.


Można zauważyć, że w punkcie 1.2 ostatnia kolumna (features) zawiera wszystkie elemnty wyposażenia występujące w danym pojeździe. Przy analizie statystycznej badanego problemu istotne jest rozdzielenie tych wartości i przypisanie ich do osobnych kolumn.
```{r}

featuresTable <- data.frame(loadedCarFromData$features)
seperatedFeatures <- separate(featuresTable, col = loadedCarFromData.features, sep = "\\|", into = paste0("w", 1:72))
seperatedFeatures <- map_df(seperatedFeatures, stri_trim_both)
uniqFeatures <- map(seperatedFeatures, unique)
uniqFeature <- unique(unlist(uniqFeatures))
matrixFeatures <- matrix(-1, ncol = length(uniqFeature), nrow = nrow(featuresTable))
for (i in seq_len(nrow(featuresTable))) {
  matrixFeatures[i,] <- as.numeric(uniqFeature %in% seperatedFeatures[i,])
}

# Kosmetyka nazw pól atrybutów
finalMatrixFeatures <- as.data.frame(matrixFeatures)
names(finalMatrixFeatures) <- stri_replace_all(uniqFeature, replacement = "_", regex = "\\s+")

# Połączenie wczytanych pojazdów wraz z odseparowanymi atrybutami (wyposażenie)
finalLoadedCar <- cbind(loadedCarFromData, finalMatrixFeatures)
```

### Usunięcie kolumny wszystkich cech oraz kolumny z url.

Mając rozdzielone wyposażenie przypisane dla każdego pojazdu można usunąć kolumnę reprezentującą wszystkie elementy wyposażenia zapisane w jednej kolumnie. Dodatkowo kolumna reprezentująca URL została również usunięta (są to niepotrzebne kolumny do analizy statystycznej).

```{r}
finalLoadedCar <- finalLoadedCar[, -c(16, 17, 23, 86)]
```

### Reprezentacja pojazdów wraz z liczbą elementów wyposoażanenia

W poniższej zmiennej została przygotowana pełna reprezentacja pojazdów wraz z oddzielonymi elementami wyposażenia.
```{r}
counterFeatures <- rowSums(finalLoadedCar[, c(20:89)])
finalLoadedCar <- cbind(finalLoadedCar, counterFeatures)
```

### Ujednolicenie waluty
W poniższym podpunkcie przygotowano jednolitą walutę dla wszystkich aut. Waluta Euro została przewalutowana do waluty PLN według kursu średniego NBP z dnia 5 maja 2020 wynoszącego 1 EUR = 4,5476 PLN

```{r}
# Ujednolicenie waluty do PLN wedlug kursu sredniego NBP z dnia 5 maja 2020: 1 EUR = 4,5476 PLN
pricePLN <- ifelse(finalLoadedCar$price_currency == "EUR", finalLoadedCar$price * 4.5476, finalLoadedCar$price)
finalLoadedCar <- cbind(finalLoadedCar[,1:15], pricePLN, finalLoadedCar[,16:90])
```

### Podział na Polskę i pozostałe kraje
W tym podpunkcie zamieniono podzielone dane ze względu na kraj pochodzenia według dwóch kryteriów: Polska lub inny.
```{r}
# Podział na Polskę i pozostałe kraje
country <- ifelse(finalLoadedCar$origin_country == "Polska", "Polska", "Inny")
finalLoadedCar <- cbind(finalLoadedCar[,1:12], country, finalLoadedCar[,13:91])
```

### Macierz z samochodami segmentu C:  "Leon", "Octavia", "Golf", "Passat", "Tipo", "Astra"

Do analizy statystycznej wybrano 8 popularnych modeli samochodów segmentu C pochodzących z lat 2010 - 2020:
* Seat Leon,
* Skoda Octavia,
* Volkswagen Golf,
* Volkswagen Passat,
* Fiat Tipo,
* Opel Astra,
* Audi A3,
* Ford Focus,

```{r}
# Wybór samochodów do analizy
# Przygotowanie macierzy z samochodami z lat 2010 - 2020 segmentu C "Leon", "Octavia", "Golf", "Passat", "Tipo", "Astra", "A3", "Focus"
modelsNameForAnalyse <- c("Leon", "Octavia", "Golf", "Passat", "Tipo", "Astra", "A3", "Focus")
extractedModels <- finalLoadedCar %>% filter(model %in% modelsNameForAnalyse) %>%
  filter(year %in% 2010:2020)
```

### Połączenie lat w pary
W celu maksymalizacji złożoności analizy statystycznej przygotowano pary lat dla całej ramki danych. Wybrano następujące pary:

* 2010 - 2011,
* 2012 - 2013,
* 2014 - 2015,
* 2016 - 2017,
* 2018 - 2019,
* 2020,

```{r}
# Połączenie lat w pary dla całej ramki danych samochodów

yearByTwoForAllExtractedModels <- ifelse((extractedModels$year == 2010 | extractedModels$year == 2011), "2010-2011",
                     ifelse((extractedModels$year == 2012 | extractedModels$year == 2013), "2012-2013",
                            ifelse((extractedModels$year == 2014 | extractedModels$year == 2015), "2014-2015",
                                   ifelse((extractedModels$year == 2016 | extractedModels$year == 2017), "2016-2017",
                                          ifelse((extractedModels$year == 2018 | extractedModels$year == 2019), "2018-2019",
                                                 "2020")))))

# Dodanie kolumny par do ramki danych
extractedModels<-cbind(extractedModels, yearByTwoForAllExtractedModels)
```


### Usunięcie pojazdów z brakiem informacji o kraju.

Pojazdy z brakiem informacji o kraju zostały usunięty z ramki danych.

```{r}
# Usunięcie pojazdów z brakiem informacji o kraju
extractedModels <-extractedModels[!(is.na(extractedModels$origin_country)),]
```

# Rozdział 2 - Analiza danych

## Średnie ceny


### Tabela podstawowych danych analizowanych modeli aut segmentu C w zależności od kraju.
W tym podpunkcie został przygotowana tabela z podstawowymi danymi takimi jak:
średnia, mediana, asymetria, kurtoza, liczebność, ilość uszkodzonych pojazdów.

```{r}
myStat <- group_by(extractedModels, model, year) %>%
  summarise(srednia = round(mean(pricePLN)),
            mediana = round(median(pricePLN)),
            asymetria = skewness(pricePLN),
            kurtoza = kurtosis(pricePLN),
            liczebnosc = n(),
            ileUszkodz = sum(no_accidents=="Nie"))
myStatTabela <- as.data.frame(myStat) %>%  arrange(year)
kable(myStatTabela)
```


### Średnia ceny analizowanych modeli aut segmentu C, w zależności od kraju oraz poszczególnych  latach.

Poniższy wykres przedstawia średnie wartości analizowanycyh modeli aut w zależności od kraju oraz poszczególnych lat.
```{r}
p <- ggplot(myStat, aes(x=factor(year), y=mediana, color=model, group=model)) + theme_bw()
p + geom_point(size=4) + geom_line(size=1) +
  scale_color_brewer(palette="Dark2") +
  labs(x="Rok produkcji", y="Mediana ceny", color="Model samochodu") +
  theme(legend.position = "top") +
  guides(color = guide_legend(nrow = 1))
chart1 <- extractedModels %>%
group_by(model, country, yearByTwoForAllExtractedModels) %>%
summarise(pricePLN = mean(pricePLN))
chart1 <- as.data.frame(chart1)


p <- ggplot(chart1, aes(x=reorder(model, pricePLN), y=pricePLN, fill=country, na.rm = TRUE)) + facet_wrap(~yearByTwoForAllExtractedModels, ncol=2)
p + geom_bar(stat="identity", position="dodge")+ scale_fill_brewer(palette="Oranges")
```
Na podstawie powyższego wykresu można zauważyć, że cena zależy od modelu pojazdu. Najdroższymi modelami są Volkswagen Passat, oraz Audi A3. Warto zwrócić uwagę na wzrost popularności modelu Fiat Tipo w ostatnich latach.

### Gęstość estymatora jądrowego dla całej ramki danych
W tym podpunkcie przygotowano dwa wykresy gęstości estymatora jądrowego dla całej ramki danych z uwzględnieniem kraju pochodzenia pojazdu.

```{r}
# Wykres gęstości estymatora jądrowego
p <- ggplot(extractedModels, aes(x=pricePLN, fill=model, color=model)) + theme_bw()
pp <- p + geom_density(alpha=0.3) + labs(x="Cena [PLN]", y="Wartość funkcji gęstości") + facet_wrap(~country, ncol=3)
pp + coord_cartesian(xlim=c(0,120000))
```
Wykres gęstości estymatora jądrowego pokazuje nam, że przygotowane dane są w pewien sposób niejednorodne (w szczególności Volkswagen Passat). Widoczne są wielomody. Dalsza analiza bez odpowiedniego 'odfiltrowania' danych nie ma sensu. Stąd w kolejnej analizie skupiono uwagę na kryteriach wpływających na ceny pojazdów.


### Wykres pudełko-wąsy dla całej ramki danych
```{r}
p <- ggplot(extractedModels, aes(x=model, y=pricePLN)) + theme_bw()
p + geom_boxplot() + labs(x="Model samochodu", y="Cena [PLN]")
```

Na powyższym wykresie pudełko-wąsy dla całej ramki danych zauważono, że Ford Focus i Volkswagen Passat
ma bardzo dużą ilość odstających obserwacji. Wartosci mediany ceny dla Volkswagen Golf, Seat Leon, Skoda Octavia,
są bardzo zbliżone do siebie. Taką samą zależność można zauważyć również w przypadku modeli Opel Astra, Ford Focus.

### Wykres pudełko-wąsy dla całej ramki danych z uwzględnieniem podziału na rok.
```{r}
p <- ggplot(extractedModels, aes(x=model, y=pricePLN)) + theme_bw() +
  facet_wrap(~yearByTwoForAllExtractedModels, scales="free", ncol=3)
p + geom_boxplot() + labs(x="Model samochodu", y="Cena [PLN]")
```
Na powyższym wykresie pudełko-wąsy dla ramki danych pojazdów z uwzględnieniem podziału na lata, dla każdego roku pojawiają się zauważalne wartości odstajace. Wynikąją one z faktu, że każdy dodatkowy podział
zmniejsza ilość analizowanych obserwacji. Podobne zjawisko związane zbliżoną wartością mediany ceny dla modeli Golf, Leon, Octavia zauważono w latach 2014-2015 oraz 2016-2017.
Dodatkowo, warto zwrócić uwagę na zależnosć wynikającą z faktu, że na cenę pojazdu wpływa rok produkcji samochodu:
* mediany cen dla poszczególnych modeli ewidentnie rosną wraz z rokiem produkcji samochodu.

### Usunięcie obserwacji odstających
Z powodu niejednorości danych, zdecydowaliśmy za pomocą funkcji identyfikujących wartości odstające zidentyfikować i usunąc te obserwacje.
```{r}
# Funkcje identyfikujące obserwacje odstające i usuń obserwacje odstające.
odstajace <- function(y){
  q1_q3 <- quantile(y, probs =c(0.25, 0.75))
  myIQR <- IQR(y)
  (y <= q1_q3[2] + 1.5*myIQR) &  (y >= q1_q3[1] - 1.5*myIQR)
}
extractedModels <- extractedModels %>% group_by(model) %>% filter(odstajace(pricePLN))
```

### Wykres gęstości estymatora jądrowego - bez obserwacji odstających
Mając już usunięte obserwacji odstające można zweryfikować wykres gęstości estymatatora jądrowego z uwzględnieniem podziału na kraj pochodzenia.
```{r}
p <- ggplot(extractedModels, aes(x=pricePLN, fill=model, color=model)) + theme_bw()
pp <- p + geom_density(alpha=0.3) + labs(x="Cena [PLN]", y="Wartość funkcji gęstości") + facet_wrap(~country, ncol=3)
pp + coord_cartesian(xlim=c(0,120000))
```

### Wykres pudełko-wąsy - bez obserwacji odstających
```{r}
p <- ggplot(extractedModels, aes(x=model, y=pricePLN)) + theme_bw()
p + geom_boxplot() + labs(x="Model samochodu", y="Cena [PLN]")
```


### Wykres pudełko-wąsy ze względu na rok - bez obserwacji odstających
```{r}
p <- ggplot(extractedModels, aes(x=model, y=pricePLN)) + theme_bw() +
  facet_wrap(~yearByTwoForAllExtractedModels,scales="free", ncol=3)
p + geom_boxplot() + labs(x="Model samochodu", y="Cena [PLN]")
```

## Analiza danych z uwzględnieniem dodatkowych filtrów
Dane pomimo odfiltrowania wartości odstających są wciąż niejednorodne. W celu usunięcie niejednorodności przygotowano odpowiednią filtrację danych.

### Przygotowanie macierzy z samochodami segmentu C - bezwypadkowe
W ninejszym podpunkcie przygotowano macierz samochodów składających się z wybranych modeli aut z  określonych przedziałów lat oraz dodatkowo uwzględniono tylko auta bezwypadkowe.

```{r}
# Odfiltrowanie aut bezwypadkowych.
extractedModelsNoAccident <-  extractedModels %>% filter(no_accidents %in% "Tak")
```
W ramach tej filtracji usunięto około 5000 aut uszkodzonych (po wypadku).

### Wykres gęstości estymatora jądrowego - bez obserwacji odstających i bez aut uszkodzonych
```{r}
p <- ggplot(extractedModelsNoAccident, aes(x=pricePLN, fill=model, color=model)) + theme_bw()
pp <- p + geom_density(alpha=0.3) + labs(x="Cena [PLN]", y="Wartość funkcji gęstości") + facet_wrap(~country, ncol=3)
pp + coord_cartesian(xlim=c(0,120000))
```

### Przygotowanie macierzy  aut bezwypadkowych, używanych, przebieg 40-150tys. km, kompakt, sedan, kombi.
W kolejnej filtracji  przygotowano dane z uwzględnieniem aut bezwypadkowych, używanych, o przebiegu 40-150tys. km oraz typu : kompakt, sedan oraz kombi.

```{r}
extractedModelsNoAccidentV1 <- extractedModelsNoAccident %>% filter(condition %in% "Uzywane") %>%  filter(type %in% "Kombi" | type %in% "Kompakt" | type %in% "Sedan") %>%  filter(country %in% "Polska") %>% filter(mileage %in%  40000:150000)
```

### Wykres gęstości estymatora jądrowego aut bezwypadkowych, używanych, przebieg 40-150tys. km, kompakt, sedan, kombi.
```{r}
p <- ggplot(extractedModelsNoAccidentV1, aes(x=pricePLN, fill=model, color=model)) + theme_bw()+ facet_wrap(~type, ncol=3)
pp <- p + geom_density(alpha=0.3) + labs(x="Cena [PLN]", y="Wartość funkcji gęstości")
pp + coord_cartesian(xlim=c(0,120000))
```

Z trzech wykresów przedstawiających wartości funkcji gęstości, można zauważyć, że najbardziej jednorodne dane posiadają modele aut typu Kombi.

### Wykres gęstości modelu aut typu kombi, nowych z uwzględnieniem kraju.
```{r}
extractedModelsNoAccidentV2 <- extractedModelsNoAccident %>% filter(type %in% "Kombi") %>% filter(condition %in% "Nowe")
# Wykres gęstości estymatora jądrowego
p <- ggplot(extractedModelsNoAccidentV2, aes(x=pricePLN, fill=model, color=model)) + theme_bw()
pp <- p + geom_density(alpha=0.3) + labs(x="Cena [PLN]", y="Wartość funkcji gęstości") + facet_wrap(~country, ncol=3)
pp + coord_cartesian(xlim=c(0,120000))
```
Na podstawie powyższego wykresu można zauważyć, że jeżeli chodzi o nowe auta pochodzenia innego niż Polska, to istnieje za mała próba by wyciągnąć ew. zależności pomiędzy danymi. Można zauważyć natomiast, że na portalu OtoMoto nowych aut z grupy interpretowanych modeli, pochodzenia innego niż polskie jest stosunkowo niewiele.

## Dystrybuanta empiryczna

### Dystrybuanta empiryczna dla aut bezwypadkowych

```{r}
p <- ggplot(extractedModelsNoAccidentV1, aes(x=pricePLN, color=model)) + theme_bw(base_size = 16)
p + stat_ecdf(size=1.5) + scale_color_brewer(palette="RdYlGn") +
  scale_y_continuous(breaks = seq(0, 1, by=0.1)) +
  scale_x_continuous(breaks = seq(0, 200000, by=20000), 
                     labels = format(seq(0, 200000, by=20000),big.mark = " ")) +
  labs(x="Cena [PLN]", y="Wartość dystrybuanty empirycznej", color="Model\nsamochodu") # \n-łamanie wiersza
```

Na podstawie tego wykresu można zauważyć, że prawdopodobieństwo zakupu danego modelu w danej cenie jest podzielone na 3 grupy. Ceny modeli można pogrupować następująco:
 I Grupa : Tipo, Astra, Focus
II Grupa : Golf, Leon, Octavia
III Grupa : A3, Passat.

Fenomenem jest tutaj II Grupa. Modele tej grupy należą do jednego koncernu motoryzacyjnego Volkswagen AG. Wyposażenie, wnętrze, wygląd nie różnią się zbytnio od siebie. Cena jak można zauważyć również.


### Dystrybuanta empiryczna dla aut bezwypadkowych w różnych latach
```{r}
p <- ggplot(extractedModelsNoAccidentV1, aes(x=pricePLN, color=model)) + theme_bw(base_size = 16)+ facet_wrap(~yearByTwoForAllExtractedModels, ncol=3)
p + stat_ecdf(size=1.5) + scale_color_brewer(palette="RdYlGn") +
  scale_y_continuous(breaks = seq(0, 1, by=0.1)) +
  scale_x_continuous(breaks = seq(0, 200000, by=20000),
                     labels = format(seq(0, 200000, by=20000),big.mark = " ")) +
  labs(x="Cena [PLN]", y="Wartość dystrybuanty empirycznej", color="Model\nsamochodu")
```
Podobny trend (podział prawdopodbieństwa cen na 3 grupy), jak w punkcie wyżej można zauważyć w latach 2014-2015, 2016-2017. W pozostałych latach ciężko ocenić zależność z powodu braku wystarczających ilości danych. Trud ten, wynika z braku odpowiedniej ilości danych w poszczególnych latach.

# Rozdział 3 - Korelacje zależności
W tym rozdziale policzono korelację zależności ceny od takich zmiennych jak wyposażenie, rocznik i przebieg.

## Zależność ceny od wyposażenia
Badanie samochodów z grupy: Polska, używane, przebieg 40-150k oraz typy kompakt, sedan, kombi

### Istotnosc statystyczna
```{r}
statSigniPriceVsFeatures <- cor.test(extractedModelsNoAccidentV1$pricePLN, extractedModelsNoAccidentV1$counterFeatures,
                                     method = "pearson")
statSigniPriceVsFeatures <- statSigniPriceVsFeatures$p.value
```
Ze względu na wartość p-value = 5.967815e-255 < 0.05, wynik jest istotny statystycznie

### Współczynnik korelacji Pearsona
```{r}
cor(extractedModelsNoAccidentV1$pricePLN, extractedModelsNoAccidentV1$counterFeatures, use = "complete.obs", method = "pearson")
```
Współczynnik korelacji Pearsona na poziomie 0.5461091 pozwala stwierdzić, że zachodzi korelacja dodatnie o umiarkowanej sile.

### Prezentacja graficzna
```{r}
ggscatter(extractedModelsNoAccidentV1, x = "pricePLN", y = "counterFeatures", add = "reg.line", conf.int = TRUE, 
          cor.coef = TRUE, cor.method = "pearson", xlab = "Cena, PLN", ylab = "Liczba elementów wyposażenia")
```

Dla samochód o ilości elementów wyposażenia równego 0 lub blieskiego 0 wyznaczona zależność ceny samochodu od ilości wyposażenia nie występuje. Najsilniejsza korelacja ceny i ilości wyposażenia występuje dla samochodów o średnim wyposażeniu.

## Zależność ceny od rocznika
Badanie samochodów z grupy: Polska, używane, przebieg 40-150k oraz typy kompakt, sedan, kombi

### Istotnosc statystyczna
```{r}
statSigniPriceVsYear <- cor.test(extractedModelsNoAccidentV1$pricePLN, extractedModelsNoAccidentV1$year,
                                     method = "pearson")
statSigniPriceVsYear <- statSigniPriceVsYear$p.value
```
Ze względu na wartość p-value = 4.724289e-134 < 0.05, wynik jest istotny statystycznie

### Współczynnik korelacji Pearsona
```{r}
cor(extractedModelsNoAccidentV1$pricePLN, extractedModelsNoAccidentV1$year, use="complete.obs")
```
Współczynnik korelacji Pearsona na poziomie 0.4108121 pozwala stwierdzić, że zachodzi korelacja dodatnia o stosunkowo niewielkiej sile.

### Prezentacja graficzna
```{r}
ggscatter(extractedModelsNoAccidentV1, x = "pricePLN", y = "year", add = "reg.line", conf.int = TRUE, 
          cor.coef = TRUE, cor.method = "pearson", xlab = "Cena, PLN", ylab = "Rok produkcji")
```

### Współczynnik korelacji Pearsona dla większej grupy
Spróbujemy jeszcze przetestować większą grupę samochodów, w której samochody powypadkowe nie są odfiltrowane, a przebieg i stan jest dowolny.
```{r}
cor(extractedModels$pricePLN, extractedModels$year, use="complete.obs")
```

Współczynnik korelacji Pearsona na poziomie 0.7626321 pozwala stwierdzić, że zachodzi korelacja dodatnia o stosunkowo wysokiej sile.

## Zależność ceny od przebiegu
Badanie samochodów z grupy: Polska, używane, przebieg 40-150k oraz typy kompakt, sedan, kombi

### Istotnosc statystyczna
```{r}
statSigniPriceVsMileageV2 <- cor.test(extractedModels$pricePLN, extractedModels$mileage,
                                      method = "pearson")
statSigniPriceVsMileageV2 <- statSigniPriceVsMileageV2$p.value
```
Ze względu na wartość p-value = 0 < 0.05, wynik jest istotny statystycznie

### Współczynnik korelacji Pearsona
```{r}
cor(extractedModels$pricePLN, extractedModels$mileage, use="complete.obs")
```
Współczynnik korelacji Pearsona na poziomie -0.6857442 pozwala stwierdzić, że zachodzi korelacja ujemna, odwrotna o stosunkowo wysokiej sile (Cena maleje, przebieg rośnie).

### Prezentacja graficzna
```{r}
ggscatter(extractedModels, x = "pricePLN", y = "mileage", add = "reg.line", conf.int = TRUE, 
          cor.coef = TRUE, cor.method = "pearson", xlab = "Cena, PLN", ylab = "Przebieg")
```
Wykres przedstawia odwrtona korelację zależności ceny od przebiegu. Widać, że dla średnich przebiegów pojazdów, korelacja zależności jest o wysokiej sile.

## Regresja liniowa

### Przygotowanie danych do analizy regresji
Samochody pogrupowane zostały na te o bogatym, średnim i podstawowym wyposażeniu.
I grupa: podstawowe wyposażenie (0-30 elementów),
II grupa: średnie wyposażenie (31-45)
III grupa: bogate wyposażenie (powyżej 45)

```{r}
extractedModelsNoAccidentV1$counterFeaturesGroup[extractedModelsNoAccidentV1$counterFeatures <= 30] <- 'podstawowe'
extractedModelsNoAccidentV1$counterFeaturesGroup[extractedModelsNoAccidentV1$counterFeatures >30 & extractedModelsNoAccidentV1$counterFeatures <= 45] <- 'srednie'
extractedModelsNoAccidentV1$counterFeaturesGroup[extractedModelsNoAccidentV1$counterFeatures > 45] <- 'bogate'
lata <- extractedModelsNoAccidentV1$yearByTwoForAllExtractedModels
wyposazenie <- extractedModelsNoAccidentV1$counterFeaturesGroup
cena <- extractedModelsNoAccidentV1$pricePLN
```


### Zależność ceny od przebiegu
```{r}
model1 <- lm(cena ~ lata)
par(mfrow=c(2,2));plot(model1);par(mfrow=c(1,1))
```


### Zależność ceny od wyposażenia
```{r}
model2 <- lm(cena ~ wyposazenie)
par(mfrow=c(2,2));plot(model2);par(mfrow=c(1,1))
```
Interpretacja ResidualsvsFitted: czerwona linia na prezentowanym wykresie jest pozioma i prosta w punkcie 0, co oznacza, że zależność pomiędzy ceną a przebiegiem jest liniowa.

Interpretacja Normal Q-Q: sprawdzamy warunek o normalnosci rozkładu reszt. Ponieważ w naszym przypadku rozkład reszt nie jest liniowy, stąd uważamy, że regresja liniowa nie jest optymalną metodą weryfikacji zależności.


Interpretacja Scale-Location: Wykres “Scale – Location” na osi poziomej przedstawia wartości dopasowane dla zmiennej objaśnianej - ceny, a na osi pionowej pierwiastki z modułów standaryzowanych reszt. W naszym przypadku punkty nie są idealnie równomiernie rozmieszczone wzdłuż osi poziomej, co oznacza brak jednorodnej wariancji.

### Zależność ceny od rocznika i wyposażenia
```{r}
interaction.plot(lata, wyposazenie, cena)
```
Na powyższym wykresie można zauważyć, że zachodzi prosta zależność pomiędzy latami, ceną, a ilością wyposażenia danego samochodu. Cena rośnie wraz z rocznikiem podobnie we wszystkich trzech grupach wyposażenia. Nie obserwuje się żadnych anomalii, takich jak niespodziewane zakłócenia ceny.


```{r}

model3 <- lm(cena~lata*wyposazenie)
anova(model3)
par(mfrow=c(1,2))
plot(model3, which = 1)
plot(model3, which = 2)
```

Zależność ceny od rocznika i wyposażenia jest zaleznością liniową (rozkład reszt jest normalny).
Ze względu na nieliniowy charakter wykresu "Normal Q-Q) możemy podważyć optymalność wybranej metody.

# Podsumowanie / Wnioski końcowe
Z powyższej analizy statystycznej badanego problemu wynika, że na cenę końcową sprzedawanych samochodów (segmentu C) wpływają następujące czynniki:
 - model pojazdu,
 - rocznik pojazdu,
 - stan pojazdu,
 - ilość wyposażenia pojazdu,
 - kraj pochodzenia pojazdu (Polska / zagranica)

 Analiza wykazała silne zalożności wymienionych czynników z ceną samochodu. W kolejnych krokach można by zastanowić się, które konkretnie elementy wyposażenia odgrywają najwiekszą rolę w kształtowaniu ceny pojazdu.